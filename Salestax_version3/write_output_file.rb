class WriteOutputFile

	def initialize(path_to_file)
        @path_to_file = path_to_file
    end  
	
	def write_output_file(item_object_array)
		require 'csv'
		CSV.open(@path_to_file,'w') do |csv|
			item_object_array.each do |items_row|
			   csv << [items_row.get_sno,items_row.get_price,items_row.get_country,items_row.get_salestax]
	        end    		
	    end	
    end	
end
