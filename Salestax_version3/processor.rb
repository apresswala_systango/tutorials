require_relative 'salestax_evaluator'
require_relative 'data_container'
class Processor

    
	def find_salestax
		  container_obj=DataContainer.instance
	      @item_object_array=container_obj.get_item_object_array
	      evaluator_obj=SalestaxEvaluator.new
	      @item_object_array.each do |items_row|
             if items_row.get_country==="india"
	    		salestax=evaluator_obj.calculate_india_salestax(items_row.get_price)
	    		items_row.set_salestax(salestax)
	    	 elsif items_row.get_country==="us"
	    		salestax=evaluator_obj.calculate_us_salestax(items_row.get_price)
	            items_row.set_salestax(salestax)
             elsif items_row.get_country==="uk"
	    		salestax=evaluator_obj.calculate_uk_salestax(items_row.get_price)
	    		items_row.set_salestax(salestax)
	    	 else
	    	    salestax=evaluator_obj.calculate_others_salestax()
	    	    items_row.set_salestax(salestax)	    				
	    	 end	
	      end
	end	

	def provide_salestax
		  container_obj=DataContainer.instance
	      @item_object_array=container_obj.get_item_object_array 
          return @item_object_array  
    end	
end	    	
