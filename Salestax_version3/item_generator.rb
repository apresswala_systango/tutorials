require_relative 'item'
require_relative 'data_container'

class ItemGenerator

	  def initialize(input_file_content)
	    @input_file_content=input_file_content
	  end
    
    def generate_item
    	item_obj=[]
      for i in 0..@input_file_content.length-1
    	 item_obj << Item.new(@input_file_content[i])
      end
      container_obj=DataContainer.instance
      container_obj.set_item_object_array(item_obj)
	end
end
