require_relative 'read_input_file'
require_relative 'item_generator'
require_relative 'processor'
require_relative 'write_output_file'

read_obj=ReadInputFile.new("Input.csv")
input_file_content=read_obj.read_input_file

item_generator_obj=ItemGenerator.new(input_file_content)
item_generator_obj.generate_item

processor_obj=Processor.new
processor_obj.find_salestax
item_object_array=processor_obj.provide_salestax

write_obj=WriteOutputFile.new("Output.csv")
write_obj.write_output_file(item_object_array)
