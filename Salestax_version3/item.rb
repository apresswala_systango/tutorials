class Item
    @sno
    @price
    @country
    @salestax

	def initialize(input_file_content)
		@sno=input_file_content[0]
		@price=input_file_content[1]
		@country=input_file_content[2]
		@salestax=""
   end

	def to_s
		return @sno.to_s + ", " + @price.to_s + ", " + @country.to_s + ", " + @salestax.to_s
	end

	def get_sno
		@sno
	end	

	def get_price
		@price
	end	
	
	def get_country
		@country
	end	
	
	def get_salestax
		@salestax
	end	
		
	def set_sno(sno)
		@sno=sno
	end

	def set_price(price)
		@price=price
	end

	def set_country(country)
		@country=country
	end

	def set_salestax(salestax)
		@salestax=salestax
	end	
end	
