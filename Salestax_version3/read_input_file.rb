

class ReadInputFile
  def initialize(path_to_file)
    @path_to_file = path_to_file
  end  


  def read_input_file
    begin
      require 'csv'
      file=open(@path_to_file)
      input_file_content=CSV.read(@path_to_file)
    rescue Exception=>e
      puts e
    end
      return input_file_content   
  end	
  
end  
