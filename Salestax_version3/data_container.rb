require 'singleton'
class DataContainer

    include Singleton
    @item_object_array

    def set_item_object_array(item_object_array)
       @item_object_array=item_object_array
    end

    def get_item_object_array
    	@item_object_array
    end

end
