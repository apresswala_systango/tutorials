class SalestaxEvaluator
   
   def calculate_india_salestax(price_str)
       price = price_str.to_i
       if price<=100
          salestax=0
       elsif price>100 && price<=500
          salestax= ((price-100)*5)/100
       else
          salestax= 20+((price-500)*20)/100
       end
          return salestax
   end
   
   def calculate_us_salestax(price_str)
       price = price_str.to_i
       salestax=Math.sqrt(price)
       return salestax
   end


   def calculate_uk_salestax(price_str)
       price = price_str.to_i
       salestax=(price*3)/100
       return salestax
   end
   
   def calculate_others_salestax()
   	   salestax="NA"
   	   return salestax
   end	   

end	
