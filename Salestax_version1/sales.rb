require 'csv'

def STax_india (price_str)
   price = price_str.to_i
   if price<=100
      salestax=0
   elsif price>100 && price<=500
      salestax= ((price-100)*5)/100
   else
      salestax= 20+((price-500)*20)/100
   end
      return salestax 
end


def STax_us (price_str)
   price = price_str.to_i

   salestax=Math.sqrt(price)
   return salestax
end


def STax_uk (price_str)
   price = price_str.to_i

   salestax=(price*3)/100
   return salestax
end


CSV.open('Output.csv','wb') do |csv|
CSV.foreach('Input.csv') do |row|
    input_file_row=row.join
    input_file_row_split=input_file_row.split(";")
   
    if input_file_row_split[2]==="india"
       salestax=STax_india input_file_row_split[1]
    elsif input_file_row_split[2]==="us"
       salestax=STax_us input_file_row_split[1]
    elsif input_file_row_split[2]==="uk"
       salestax=STax_uk input_file_row_split[1]
    else 
       salestax="NA"     
    end
      csv << [input_file_row_split[0],input_file_row_split[1],input_file_row_split[2],salestax]    
end 
end 
 
