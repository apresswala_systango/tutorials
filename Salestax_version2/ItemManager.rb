require_relative 'SalestaxEvaluator'

class ItemManager

	def initialize(input_content)
		@input_file_content=input_content
	end
		
	
	def find_salestax()
		stax_index=3
	    incr=0
	    evaluator_obj=SalestaxEvaluator.new
	    @input_file_content.each do |sno,price,country,salestax|
	    	if country==="india"
	    		@input_file_content[incr][stax_index]=evaluator_obj.salestax_india(price)
	    		incr+=1
	    	elsif country==="us"
	    		@input_file_content[incr][stax_index]=evaluator_obj.salestax_us(price)
	    		incr+=1
            elsif country==="uk"
	    		@input_file_content[incr][stax_index]=evaluator_obj.salestax_uk(price)
	    		incr+=1
	    	else
	    	    @input_file_content[incr][stax_index]=evaluator_obj.salestax_other()
	    	    incr+=1		    				
	    	end	
	    end
	end

	def provide_salestax()
		return @input_file_content
    end		
end	    	