class ReadInputFile
  def initialize(path_to_file)
    @path_to_file = path_to_file
  end  

  def is_file_exist 
    begin
  	  file=open(@path_to_file)
    rescue
        puts "The given file not found"
    end   
  end

  def is_file_readable
     begin
      file=File.stat(@path_to_file).readable?
     rescue
        puts "The given file is not readable"
     end
  	 
  end

  def read_input_file
  	require 'csv'
  	input_file_content=CSV.read(@path_to_file)
  	return input_file_content
  end	
end  