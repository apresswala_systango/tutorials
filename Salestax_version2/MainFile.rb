require_relative 'ReadInputFile'
require_relative 'ItemManager'
require_relative 'WriteOutputFile'

read_obj=ReadInputFile.new("Input.csv")
read_obj.is_file_exist()
#read_obj.is_file_readable()
from_ip_file=read_obj.read_input_file()

manager_obj=ItemManager.new(from_ip_file)
manager_obj.find_salestax()
to_op_file=manager_obj.provide_salestax()

write_obj=WriteOutputFile.new("Output.csv")
write_obj.write_output_file(to_op_file)