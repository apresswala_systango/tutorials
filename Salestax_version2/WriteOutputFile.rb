class WriteOutputFile

	def initialize(path_to_file)
        @path_to_file = path_to_file
    end  
	
	def write_output_file(output_file_content)
		require 'csv'
		CSV.open(@path_to_file,'w') do |csv|
			output_file_content.each do |sno,price,country,salestax|
			  csv << [sno,price,country,salestax]
			end    		
	    end	
    end	
end